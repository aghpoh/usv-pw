from django.conf.urls import url
from events import views, views_api, auth_views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^presenter$', views.presenter_index, name='presenter_index'),
    url(r'^create$', views.create, name='create'),
    url(r'^presenter/create$', views.presenter_create, name='presenter_create'),
    url(r'^save$', views.save, name='save'),
    url(r'^presenter/save$', views.presenter_save, name='presenter_save'),
    url(r'^edit/todos/(\d+)$', views.edit, name='edit'),
    url(r'^edit/presenter/(\d+)$', views.presenter_edit, name='presenter_edit'),
    url(r'^delete/presenter/(\d+)$', views.presenter_delete, name='presenter_delete'),
    url(r'^delete/todos/(\d+)$', views.delete, name='delete'),
    url(r'^complete$', views.complete, name='complete'),

    # API Route for Ajax
    url(r'^api/todos/(?P<pk>[0-9]+)$', views_api.TodoItemView.as_view(), name='api_todo_item'),
    url(r'^api/todos$', views_api.TodoListView.as_view(), name='api_todo_list'),

    # Auth Routes
    url(r'^login$', auth_views.login, name='login'),
    url(r'^authenticate$', auth_views.authenticate, name='authenticate'),
    url(r'^logout$', auth_views.logout, name='logout'),
    url(r'^signup$', auth_views.signup, name='signup'),
    url(r'^signup/submit$', auth_views.signup_submit, name='signup-submit'),
]
